//HAOSEN HAN; CS201; SEC 3 ;Master of Computer Science(Beacon China Online);


//Question Two:
//Write a program that prompts the user for an integer. 
//If the integer is divisible by 2 print out "foo", 
//and if the integer is divisible by 3 print out "bar".
//If the integer is divisible by both, your program should print out "foobar" 
//and if the integer is not divisible by either, 
//then your program should not print out anything.




package exams.first;

import java.util.Scanner;

public class QuestionTwo {

	public static void main(String[] args) {
		
		
		
		System.out.print("Input an integer:  ");
		
		Scanner input=new Scanner(System.in);
		
		
		try{
			String line=input.nextLine();
			
			int num=Integer.parseInt(line);
			
			
			
	       if ((num%2==0)&&(num%3==0)) {
				
				System.out.println("foobar");
			}
			
			
	       else {
	    	   
	    	if (num%2==0) {
				
				System.out.println("foo");
			}
			
			
			 if(num%3==0) {
				
				System.out.println("bar");
			}
			
	       }
		
			
			
			
		}
		catch(Exception e) {
			
			System.out.println(e.getMessage());
			System.out.println("Sorry, it's an invalid input. ");
		}
		
		

		
		input.close();

	}

}
