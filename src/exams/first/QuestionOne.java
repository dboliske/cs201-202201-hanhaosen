//HAOSEN HAN; CS201; SEC 3 ;Master of Computer Science(Beacon China Online);


//Question One
// Write a program that prompts the user for an integer, 
// add 65 to it, convert the result to a character 
// and print that character to the console.




package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		

		
		System.out.print("Input an integer:  ");
		
		Scanner input=new Scanner(System.in);
		
		String line=input.nextLine();
		
		try{
			
		int number=Integer.parseInt(line);
		
		int total=number+65;
		
		char c=(char)total;
		
		System.out.println(c);}
		
		catch(Exception e) {
			
			System.out.println("Sorry, it's an invalid input.");
			
		}

        input.close();
	}

}
