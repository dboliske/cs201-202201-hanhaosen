//HAOSEN HAN; CS201; SEC 3 ;Master of Computer Science(Beacon China Online);



//Question Three

//Write a program that prompts the user for an integer and then prints out a Triangle of that height and width. 
//For example, if the user enters 3, then your program should print the following:



package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		
		
		
		System.out.print("Input an integer:  ");
		
		Scanner input=new Scanner(System.in);
		
		try{
			
			int size=Integer.parseInt(input.nextLine());
			

			
			
			
			for(int i=0; i<size;i++) {
				
				for(int j=0;j<size;j++) {
								
				if(j<i) {
					System.out.print("   ");
				} else {
					System.out.print(" * ");
					
				}
				}
				System.out.println();
				
			}
			
			
			input.close();
			
		} catch(Exception e) {
			
			System.out.println(e.getMessage());
			
			System.out.println("Invalid input");
		}
		
		
		
		
		
		
		
		

	}

}
