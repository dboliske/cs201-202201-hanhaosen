//HAOSEN HAN; CS201; SEC 3 ;Master of Computer Science(Beacon China Online);


//QuestinFive:

//Write a Java class based on the following UML diagram. 
//Include all standard methods, 
//such as constructors, mutators, accessors, toString, and equals. 
//Additionally, implement any other methods shown in the diagram.
//NOTES:age needs to be positive.
//You donotneed to write an application class.



package exams.first;



	public class Pet  {
		
		private  String name;
		
		private  int age;
		
		
		
		public Pet(){
			
			name="dundun";
			
			age=1;
			
		}
		
		public Pet(String name, int age) {
			
			this.name="dundun";
			setName(name);
			
			this.age=1;
			setAge(age);
			
		}
		
		
		
		public void setName(String name) {
			
			this.name=name;
		}
		
		
		public void  setAge(int age) {
			
			if (age>0) {
			this.age=age;
		}
		
		}

		
		
		public String getName() {
			
			return this.name;
		}
		
		
		
		public int getAge() {
			
			return this.age;
		}
		
		

		
		
		public boolean equals(Object obj) {
			
			if (! (obj instanceof Pet)) {
				
				return false;
			}
			
			else    {
	
			Pet p=(Pet)obj;
			
			if (!this.getName().equals(p.getName())) {
				return false;
			}
			
			else if (this.age!=p.getAge()) {
				return false;
			}
		
			else {	
				
			return true;
			
			 }
		   }
		}
		
		
		
		
		public String toString() {
			
			
			return name+" is "+age+" years old pet.";
		}
		
		
		
		
		
		
		
		
		
		


}
