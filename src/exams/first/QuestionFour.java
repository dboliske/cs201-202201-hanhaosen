//HAOSEN HAN; CS201; SEC 3 ;Master of Computer Science(Beacon China Online);


//      Question Four:
//		Write a program that prompts the user for 5 words and prints out any word that appears more than once.
//
//		NOTE:
//
//		The words should be an exact match, i.e. it should be case sensitive.
//		Your programmustuse an array.
//		Do not sort the data or use an ArrayLists.





package exams.first;

import java.util.Scanner;

public class QuestionFour {

	public static void main(String[] args) {
		
		
        Scanner input= new Scanner(System.in);
		
		String [] line= new String [5];
		
		String [] repeat= new String[5];
			    
	    
		for (int i=0; i<5; i++) {       // create an array to allocate words
			
			System.out.print("Input a word: ");
			
			line[i]=input.nextLine();
			
		}
		
		input.close();
        
		
		
		
	
		
		
		for(int i=0;i<5;i++) {             // allocate the words that occurred more than once  to a new array
			
			for (int j=i+1;j<5;j++) {
				
				if (line[i].equals(line[j])) {
					
		        repeat[i]=line[i];
		       
	   
				}
			}
		}
		
		
		
		
		
		
		for(int i=0;i<5;i++) {
			
			for(int j=i+1;j<5;j++) {
				
				if((repeat[i]!=null) && repeat[i].equals(repeat[j])) {
					
					repeat[j]=null; // delete the duplicate 
				}
				
			}		
			
		}
		
		
		
		
		
		
		
		
		for(int i=0;i<5;i++) {  // print out
			
			if(repeat[i]!=null) {
				
				System.out.println(repeat[i]);
			}
		}
		
		
	
	
		
        
        
        
	}

}
