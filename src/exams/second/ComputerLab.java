//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 1

package exams.second;

public class ComputerLab extends Classroom {

	private boolean computers;
	
	public ComputerLab() {
		
		super();
		computers=false;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	@Override
	public String toString() {
		return super.toString()+"  "+" Has computers "+computers;
	}
	
	
	
	
	
	
	
}
