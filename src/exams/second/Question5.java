//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 5



package exams.second;


import java.util.Scanner;

public class Question5 {

	public static int  jumpSearch(double []a, double b) {
		
	 
	 
	 int f=searchR(a,b,0,(int)Math.sqrt(a.length));    //recursion start;  Compare to start at a[0],   step to be (int)Math.sqrt(a.length)
	 if(f==-1) {
		 System.out.println("-1");
//		 System.out.println(b+" not found");   // not sure whether this should be shown in the console
		 
	 }
	 
	 return f;
		
	}
			
	public static int searchR(double []a,double b,int prev, int step) {	
			
		        int t=-1;
		        
		        if (b<a[0]||b>a[a.length-1]) {
		        	return -1;
		        }else {
		        
		       	if(a[prev]<b && (prev+step<a.length-1) ) {
		 		
		       	 t=searchR(a,b,prev+step,step);            // recursion 
		       	} 
		       	
		       	else if (a[prev]<b && (prev+step)>=a.length-1) {   // to check one by one in the  last range
	
		       		for (int i=prev;i<a.length;i++) {
		    			if(a[i]==b) {
		    				System.out.println(b+" found at index of "+i);
		    				return i;
		    			}
		       		}	return -1;
		       	
			    }
		       	
		       	
		       	else  if (a[prev]==b){
		       		System.out.println(b+" found at index of "+prev);
		       		return prev;
			    } 
		       	
		       	
		       	else if (a[prev]>b ) {   // to check one by one within the range
			    
			    		for (int i=prev;i>prev-step;i--) {
			    			if(a[i]==b) {
			    				System.out.println(b+" found at index of "+i);
			    				return i;
			    			}	
			    		}	return -1;
			 
		       		}
		       	    return t;
		       	}      	
		   }    	
		

	
	
	
	public static void main(String[]args) {
		
		
		double [] a={0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		
		System.out.println("Input a number to search: ");
		
		Scanner input=new Scanner(System.in);
		String n= input.nextLine();
		
		try {
			double d=Double.parseDouble(n);
			
			int t=jumpSearch(a,d);
			
			
			
		}catch (Exception e){
			
			System.out.println("Invalid input, please enter in a number");
		}
		
		
		
		input.close();
	}
	
	
	
	
	
	
	
	
	
}
