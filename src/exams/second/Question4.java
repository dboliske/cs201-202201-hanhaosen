//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 4


package exams.second;

import java.util.ArrayList;
import java.util.Collections;

public class Question4 {

	
		
		
		public static ArrayList<String> selectionSort(ArrayList<String> a){
			
			
			for(int i=0;i<a.size();i++) {
				
				for(int j=i+1;j<a.size();j++) {
					if(a.get(i).compareTo(a.get(j))>0) {
						Collections.swap(a, i, j);
		
					}
				}	
			}		
			
			return a;
		}
		
		
		public static void main(String[]args) {
			
			String []line ={"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
			ArrayList<String> a=new ArrayList<String>();
			for(String value: line) {
				a.add(value);
			}
			
			a=selectionSort(a);
			System.out.println(a);
			
		}



}
