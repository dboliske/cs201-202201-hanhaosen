//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 3


package exams.second;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Scanner;

public class Question3 {

	public static void main(String[]args) {
	
	ArrayList<Double> a=new ArrayList<Double>();
	Scanner input=new Scanner(System.in);
	boolean flag=true;
	System.out.println("To exit program enter Done");
	do {
		
		System.out.println("Please Enter a number: ");
	  
	    	String t=input.nextLine();
	    	if (t.equals("Done")) {
	    		flag=false;
	    		break;
	    	}

	    	try {
	    	double d=Double.parseDouble(t);
	    	a.add(d);
	        }catch(Exception e) {
	        	System.out.println("invalid input");
	        }

	}while(flag);

	input.close();
	
	if (a.size()!=0) {

	double min=a.get(0);
	for(int i=0;i<a.size();i++) {
		if(a.get(i)<min) {
			min=a.get(i);
		}
	}
	System.out.println("Minimum entered is : "+min);
	
	Collections.sort(a);// can find the max the same way as the min, however an easier way to find the maximum by using the sort method
	
	System.out.println("Maximum entered is "+a.get(a.size()-1));
	
		}
	}
	
}
