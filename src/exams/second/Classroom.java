//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 1


package exams.second;

public class Classroom {

	protected String  building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		
		building="Stuart";
		roomNumber="1";
		seats=1;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String number) {
		this.roomNumber = number;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats>0) {
		this.seats = seats;
		}
	}
	public String toString() {
		return "Building name "+building+"  Room number "+roomNumber+"  Seats number "+seats;
	}
	
	
	
}
