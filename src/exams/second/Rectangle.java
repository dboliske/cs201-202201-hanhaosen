//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 2


package exams.second;

public class Rectangle extends Polygon {

	private double width;
	private double height;
	
	
	public Rectangle() {
		super();
		width=1;
		height=1;	
	}
	
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if(width>0) {
		this.width = width;
		}
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		if(height>0) {
		this.height = height;
		}
	}


	@Override
	public String toString() {
		return super.toString()+ " Rectangle [width=" + width + ", height=" + height + "]";
	}

	@Override
	public double area() {
		
		return height*width;
	}
 
	
	public double perimeter() {	
		return 2*(height+width);
	}

}
