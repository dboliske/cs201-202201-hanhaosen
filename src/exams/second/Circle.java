//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 2


package exams.second;

public class Circle extends Polygon {

	private double radius;
	
	public Circle() {
		super();
		radius=1;
	}
	

	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		if(radius>0) {
		this.radius = radius;
		}
	}

	
	
	

	@Override
	public String toString() {
		return super.toString()+ " Circle [radius=" + radius + "]";
	}


	@Override
	public double area() {
		
		return Math.PI*radius*radius;
	}

	public double perimeter() {
		return 2*Math.PI*radius;
	}

}
