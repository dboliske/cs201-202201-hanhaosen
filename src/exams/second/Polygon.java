//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)
// Question 2




package exams.second;

public abstract class Polygon {

	protected String name;
	
	public Polygon() {
		this.name="polygon a ";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Polygon [name=" + name + "]";
	}

	
	public abstract double area();
	public abstract double perimeter();
	
	
	
}
