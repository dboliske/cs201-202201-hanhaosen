//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)

// APP2 is the code for the client to use, like adding or listing goods for the store


package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.regex.Pattern;

public class APP2 {

	
	public static ArrayList<Item> loadFile(ArrayList<Item>a, Scanner input) throws FileNotFoundException  {// to load new file or existing file
		
		System.out.println("Type in the file address:  ");
		String ll=input.nextLine();
		try{
		File f=new File(ll);
		
		input=new Scanner(f);
		
		while(input.hasNextLine()) {
			
			try{
			String line=input.nextLine();
			String[]value=line.split(",");
			int n=value.length;
		
			Item i=null;
			switch(n) {
			case 2:
				 i=new Item(value[0],Double.parseDouble(value[1]));// to identify general items from produce item or restriction item
				 break;
			case 3:
				String datePattern1="([1-9]|[0][1-9]|[1][0-2])(-|/)"
						+ "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
						+"([0-9]+)";
				
			    String datePattern2 =  "(\\w+)(-|/|\\s+|,)"
						+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/|\\s+|,)"
						+ "([0-9]+)";
				
				
				if((Pattern.matches(datePattern1,value[2]))||(Pattern.matches(datePattern2,value[2]))) {// to select produce item either in several different format
				 i=new ProduceItem(value[0],Double.parseDouble(value[1]),Date.parseDate(value[2]));    // such as 04/29/2022, 4/29/2022, April 29 2022 or April 29/2022
				}else {
				 i=new RestrictionItem(value[0],Double.parseDouble(value[1]),Integer.parseInt(value[2]));
				}
				break;
			}	
			a.add(i);	
			}catch(Exception e) {
				System.out.println("Reading error occurred");
			}
		 }
		}catch(FileNotFoundException e) {
			System.out.println("File not Found");
			
		}
		return a;
	}
	

	
	
	
	public static void saveToFile( ArrayList<Item> a,Scanner input) {  // to save file to a new file 
		
		try {
			
			System.out.println("Type in the new file address:  ");
			input=new Scanner(System.in);
			String filename=input.nextLine();
			FileWriter f=new FileWriter(filename);
			
			for(Item i:a) {
				f.write(i.toCSV()+"\n");
			}
			f.flush();
			f.close();
			
		} catch (IOException e) {
			System.out.println("Invalid file address");
			
		} catch(Exception e) {
			
		}
	}
	

	
	public static ArrayList<Item> addItem(ArrayList<Item>a, Scanner input ){  // to add new item
		
	
		input=new Scanner(System.in);
		System.out.println("Set item name: ");
		String name=input.nextLine();
		System.out.println("Set item price: ");
		try {
		Double price=Double.parseDouble(input.nextLine());
		
		System.out.println("Item type: \n1.Produce Item\n2.Restriction Item\n3.General Item"); // to add new item into 3 categories
		String c=input.nextLine();
		switch(c) {
		
		case"1":
			System.out.println("Type in expiration date in formate MM/dd/yyyy");
			String edate=input.nextLine();
			a.add(new ProduceItem(name,price,edate));
			break;
		case"2":
			System.out.println("Type in restriction age: ");
			String rage=input.nextLine();
			int r=Integer.parseInt(rage);
			a.add(new RestrictionItem(name,price,r));
			break;
		case"3":
			a.add(new Item(name,price));
			break;
			
		default:
			System.out.println("Sorry, your input is not recognized");
		
		}
		}catch(Exception e) {
			System.out.println("Your input is invalid");
		}

		return a;
	}
	
	public static ArrayList<Item>  deleteItem(ArrayList<Item>a, Scanner input){ // to delete item
		
	   
		ArrayList<Integer> t=findItem(a,input);// use method findItem to find item index, increasing code efficiency
		System.out.println("index of items:"+t);
		
		if(t.size()!=0) {
		System.out.println("To delete all the items above?  y/n");
		String va=input.nextLine();
		switch(va) {
		
		case"y":
			Collections.sort(t); // to sort the index from smallest integer ,upwards
			int qq=t.size();
		    for (int i=t.get(0);qq!=0;qq--) {  // use loop to remove item at the smallest index every time to guarantee loop size changes does not incur "out of boundary " error
		    
				a.remove((int)t.get(0));
		    }
			
			break;
		case"n":	
			break;
        default:
        	System.out.println("Your  input is not recongnized");
		}
		}
		return a;
	}
	
	
	public static ArrayList<Item> sellItem(ArrayList<Item>a,Scanner input){  // to sell item
		
		ArrayList<Item>b=new ArrayList<Item>();
		boolean flag=true;
		do {
		ArrayList<Integer> t=findItem(a,input);
		
		
		if(t.size()!=0) {
		System.out.println("number of items :"+t.size());
		

		System.out.println("Type in the number you want ?");
		String nu=input.nextLine();
		try {
		int num=Integer.parseInt(nu);
		Collections.sort(t); 
		int qq=t.size();
		
		if (num>qq) {
			System.out.println("Sorry, the number is out of stock for this item");
		}
		else if(num>0&&num<=qq) {
			
	    for (int i=t.get(0);num!=0;num--) {  // use loop to remove item at the smallest index every time to guarantee loop size changes does not incur "out of boundary " error
	    
	    	if(!(a.get((int)t.get(0)) instanceof RestrictionItem)) {
	    		
	    		b.add(a.get((int)t.get(0))); // add the to-sell item to b 
	    		
	    		
	    		a.remove((int)t.get(0));
	    	}
	    	else {
	    		
	    	
	    		RestrictionItem r=(RestrictionItem)a.get((int)t.get(0));
	    		System.out.println("Restriction Age:"+r.getAgeRes());
	    		if (r.isEligible(input)) {
	    			b.add(a.get((int)t.get(0))); // add the to-sell item to b 
	    			a.remove((int)t.get(0));
	    		}else {
	    			System.out.println("Sorry, customer is not permitted to buy");
	    			}
	    		
	    		}
			
	    	}
		}
	
		System.out.println("check out ? \n1. yes\n2. no\n3. cancel order?");
		
	    String ch=input.nextLine();
	    
		switch(ch) {
			case"1":
				flag=false;
				break;
			case"3":
				flag=false;
				for(Item i: b) {
					a.add(i);
				}	
				b=null;
				break;
				
			case"2":
			default:
				break;
		}
		}catch(Exception e) {
			System.out.println(" Input is not valid. Please enter an integer ");
			
		}
		}else {
			
		}
		
		}while(flag);
		
		if(b!=null) {
		System.out.println("Items sold: ");
		
		for(Item i: b) {
			System.out.println(i);
		}
		}
		return a;
	}
	
	
	
	
	
	public static ArrayList<Item>  clearItems(ArrayList<Item>a, Scanner input){ // to clear all items in the arraylist
		
		System.out.println("To delete all items ?  y/n");
		
		String va=input.nextLine();
		switch(va) {
		
		case"y":
			a.clear();
			break;
		case"n":	
			break;
        default:
        	System.out.println("Sorry, your input is not recongnized");
		}
		
		
		return a;
	}
	
	
	
	
	
	
	
	public static void listItems(ArrayList<Item>a,Scanner input) {// to list all items in the arraylist
		
		System.out.println("1.listAllitems\n2.listProduce\n3.listItemNotFresh\n4.listRestrictionItem");
		input=new Scanner(System.in);
		String c=input.nextLine();
		switch(c) {
		
		case"1":
			listAllItems(a);  // list all items
			break;
		case"2":
			listProduceItems(a);// list produce items
			break;
		case"3":
			listItemNotFresh(a);// list items that not fresh(compare the best-before date vs the current searching  date)
			break;
		case"4":
			listRestrictionItems(a);// list all restriction items
			break;
		default:
			System.out.println("Sorry, your choice not recongnized");
		    break;
		
		}
		
	}
	
	
	
	public static void  listAllItems(ArrayList<Item>a){// list all items
		
		System.out.println("Items will be sorted on item name's alphabetical order");
		sort(a);
		for(int i=0;i<a.size();i++) {
			System.out.println(i+" "+a.get(i));
		}
		
	}
	
	public static void listProduceItems(ArrayList<Item>a) {// list produce items
		System.out.println("Items will be sorted on item name's alphabetical order");
		sort(a);
		for(int i=0;i<a.size();i++) {
		
		if(a.get(i)instanceof ProduceItem) {//  to select items that belong to the produce item category
			System.out.println(i+" "+a.get(i));
		}
		}
	}
	
     public static void listItemNotFresh(ArrayList<Item>a) {// list items that out of best-before-date
    	System.out.println("Items will be sorted on item name's alphabetical order");
		sort(a);
		ArrayList<ProduceItem> p=new ArrayList<ProduceItem>();
		for(int i=0;i<a.size();i++) {
		if(a.get(i)instanceof ProduceItem) {//  to select items that belong to the produce item category
			p.add((ProduceItem) a.get(i));
			}
		}
		for(ProduceItem o: p) { // to select not fresh items
			if (!o.fresh()) {
				System.out.println(o);
			}
		}
		
	}
	
	
	
	public static void listRestrictionItems(ArrayList<Item>a) { // to list all restriction items
		System.out.println("Items will be sorted on item name's alphabetical order");
		sort(a);
		for(int i=0;i<a.size();i++) {
		
		if(a.get(i)instanceof RestrictionItem) {
			System.out.println(i+" "+a.get(i));
		}
		}
	}
	
	
	public static ArrayList<Item> sort(ArrayList<Item> a){  // to quick sort the array list for use of search and other functions
		
		return QuickSort.sort(a);
	}
	
	
	
	public static ArrayList<Integer> findItem(ArrayList<Item> a, Scanner input) { // to search an item and return the index of items in the arraylist
		
		System.out.println("Type in  item: ");
		input=new Scanner(System.in);
		String b=input.nextLine();
		ArrayList<Integer> t=new ArrayList<Integer>();
		a=sort(a);												// ensure the array list is sorted before search
		int start = 0;
		int end = a.size();
		int pos = -1;
		boolean found = true;
		while (found && start != end) {								// use binary search to find at least one item 
			int middle = (start + end) / 2;
			if (a.get(middle).name.equalsIgnoreCase(b)) {
			    found = false;
				pos = middle;
				t.add(pos);
				System.out.println(b+ " Found");
				System.out.println(a.get(pos));
				
				int t1=pos;													
				boolean flag1=true;
				int tt=t1-1;
				while(tt>=0&&a.get(tt).name.equals(b)&& flag1) {				// since the sequence is sorted, just check the items besides the  found above item to increase efficiency
					t.add(tt);
					System.out.println(a.get(tt));
					tt--;														// check the neighboring item downwards
					
					if(tt<0||!(a.get(tt).name.equals(b))) {
						flag1=false;
					}
					
				}
				int t2=pos;
				
				boolean flag2=true;
				int td=t2+1;
				while(td<a.size()&&a.get(td).name.equals(b)&& flag2) {			//	check the neighboring item upwards
					t.add(td);
					System.out.println(a.get(td));
					td++;
					if(td>=a.size()||!(a.get(td).name.equals(b))) {
						flag2=false;
					}
					
				}
	
			} else if (a.get(middle).name.compareToIgnoreCase(b) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		if(pos==-1) {
			System.out.println(b+ " not found");
		}
		return t;
	}
	
		
	public static ArrayList<Item> modifyItem(ArrayList<Item> a, Scanner input){			// to modify to item
		
		
		ArrayList<Integer> t=findItem(a,input);
		if(!t.isEmpty()) {
		
		System.out.println("1.modify name\n2.modify price\n3.modify expirationDate;\n4.modify restriction age\nwhat's your choice?");
		String c=input.nextLine();
		
		switch(c) {
		
		case"1":
			System.out.println("Type in the new name: ");
			String newname=input.nextLine();
			for(Integer i: t) {
				a.get(i).setName(newname);
			}
			break;
		case"2":
			System.out.println("Type in the new name: ");
			String newprice=input.nextLine();
			for(Integer i: t) {
				a.get(i).setPrice(Double.parseDouble(newprice));
			}
			break;
		case"3":
			System.out.println("Type in the new expirationDate: ");
			String newdate=input.nextLine();
			
			for(Integer i: t) {
				if(a.get(i)instanceof ProduceItem) {
					ProduceItem p=(ProduceItem)a.get(i);
					p.setExpirationDate(newdate);		//	 if the new date format doesn't fit , it will return a null expiration date to the item
					a.set(i, p);		
					}
				}	

			break;
		case "4":
			
			System.out.println("Type in the new restriction age: "); // to set a new restriction age
			String newage=input.nextLine();
			
			try{
				int ta=Integer.parseInt(newage);			//	to ensure if the input is not a valid integer, it will not change anything 
			
				for(Integer i: t) {
				if(a.get(i)instanceof RestrictionItem) {
					RestrictionItem q=(RestrictionItem)a.get(i);
					
						q.setAgeRes(ta);
						a.set(i, q);		
					}	
				}
			}catch(Exception e) {
				System.out.println("Input is not valid");
			}
			break;
		default:
			System.out.println("Sorry, your choice is not recognized");
		
			}
		}
		   return a;
		}
		
	

	
	
		
	public static ArrayList<Item> menu(ArrayList<Item>a, Scanner input) throws FileNotFoundException{      // to provide menu options for clients
		
		 boolean flag=true;
		 input=new Scanner(System.in);
		 
		 do {
		String line="1.loadFile\n2.addItem\n3.listItem\n4.findItem\n5.modifyItem\n6.deleteItem\n7.clearItem\n8.sellItem\n9.saveToFile\n10.exit\nWhat's your option?";
		System.out.println(line);
	
		String value=input.nextLine();			
		
		switch(value) {
		
		case"1":
			loadFile(a,input);
			break;
		case"2":
			addItem(a,input);
			break;
        case"3":
			listItems(a,input);
			break;
        case"4":
        	ArrayList<Integer> t=findItem(a,input);
        	System.out.println("index of items:"+t);
			break;
		
        case"5":
        	modifyItem(a,input);
			break;
		case"6":
			deleteItem(a,input);
			break;
        case"7":
			clearItems(a,input);
			break;
        
        case"8":
			sellItem(a,input);
			break;   
        case"9":
			saveToFile(a,input);
			break;
        case"10":
        	System.out.println("See you next time");
        	flag=false;
        	break;
		default:
			System.out.println("Sorry, your choice is not recognized");
			break;
			};
		   }while(flag);
		
			return a;
		
	}
		
	
	
	
	
	public static void main(String[] args) throws FileNotFoundException  {  // to operate 
		
		ArrayList<Item>a=new ArrayList<Item>();
	    Scanner input=new Scanner(System.in);
		
		menu(a,input);
	    
	    

		input.close();

		
		
		
	}

}
