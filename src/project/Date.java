//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)


package project;

import java.util.regex.Pattern;

public class Date implements Before{// this class will be used by the restrictionItem class  (like birth of date ) and produceItem class( like  best-before date)
	
	private int year;
	private int month;
	private int day;
	
	public Date(){
		
		year=2100;
		month=1;
		day=1;	
	}
	
	private static String datePattern1 =  "([1-9]|[0][1-9]|[1][0-2])(-|/)"
			+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
			+ "([0-9]+)";
	
	private static String datePattern2 =  "(\\w+)(-|/|\\s+|,)"
			+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/|\\s+|,)"
			+ "([0-9]+)";
	
	
	public Date(int month, int day, int year) {
		this();
		setMonth(month);
		setDay(day);
		setYear(year);
	}	

	public Date(Month m, int day, int year) {
		this();
		setMonth(m);
		setDay(day);
		setYear(year);
	}	

	public Date(String d) {
		this();
		if (Pattern.matches(datePattern1, d)) {  // 04/29/2022   4/29/2022
			String[] value1 = d.split("-|/");
			
			setYear(Integer.parseInt(value1[2]));
			setMonth(Integer.parseInt(value1[0]));
			setDay(Integer.parseInt(value1[1]));
		}
		else if(Pattern.matches(datePattern2,d)) {    // April 29 2022, APR. 29 2022
			
			String [] value2=d.split("-|/|\\s+|,");
			setYear(Integer.parseInt(value2[2]));
			setDay(Integer.parseInt(value2[1]));
			String m=value2[0].toUpperCase();
			
			switch(m) {
			
			case "JANUARY":
			case "JAN.":
				setMonth(1);
				break;
			case "FEBRUARY":
			case "FEB.":
				setMonth(2);
				break;
			case "MARCH":
			case "MAR.":
				setMonth(3);
				break;
			case "APRIL":
			case "APR.":
				setMonth(4);
				break;
			case "MAY":
				setMonth(5);
				break;
			case "JUNE":
			case "JUN.":
				setMonth(6);
				break;
			case "JULY":
			case"JUL.":
				setMonth(7);
				break;
			case "AUGUST":
			case "AUG.":
				setMonth(8);
				break;
			case "SEPTEMBER":
			case"SEP.":
			case"SEPT.":
				setMonth(9);
				break;
			case "OCTOBER":
			case"OCT.":
				setMonth(10);
				break;
			case "NOVEMBER":
			case "NOV.":
				setMonth(11);
				break;
			case "DECEMBER":
			case "DEC.":
				setMonth(12);
				break;
			}
	
		}
	}

	
	
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if (month>0&& month<13) {
			this.month = month;
		}
	}
	
	public void setMonth(Month m) {
		
		int n=Month.getMo(m);
		if (n!=-1) {
			this.month=n;
		}
	}
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		
		if(day>0 && day<=Month.daysOfMonth(month)) {
			this.day = day;
		}
	}

    public String getDate() {
		
		return month+"/"+day+"/"+year;
	}
	
	@Override
	public String toString() {
		
		return month+"/"+day+"/"+year;
	}
	@Override
	public boolean equals(Object obj) {
		
		if(obj==null) {
			return false;
		} else if (!(obj instanceof Date)) {
			return false;
		}
		Date d=(Date)obj;
		if (this.year!=d.getYear()) {
			return false;
		}else if(this.month!=d.getMonth()) {
			return false;
		}else if (this.day!=d.getDay()) {
			return false;
		}
		return true;
	}
	
	@Override
	
    public boolean before(Object obj) {// to judge which date is earlier
		
		if(obj==null) {
			System.out.println("Sorry, There's nothing to compare ");
			return false;
		} else if (!(obj instanceof Date)) {
			System.out.println("Sorry, this is from outside invalid Date Class");
			return false;
		}
		try {
			Date d=(Date)obj;
	
		if (this.year>d.getYear()) {
			return false;
		}else if(this.year==d.getYear()&& this.month>d.getMonth()) {
			return false;
		}else if (this.year==d.getYear()&& this.month==d.getMonth()&&this.day>=d.getDay()) {
			return false;
		}
		return true;
		
		}catch(Exception e) {
			
			System.out.println("Can't TypeCasting Object, Change Date Format");
			return false;
		}
		
	}
	
	public static Date parseDate(String d) {// to change data type from String to Date
		
		if(Pattern.matches(datePattern1, d)) {
			return new Date(d);
		}else if(Pattern.matches(datePattern2, d)) {
			return new Date(d);
		}
		
		return null;
	}
	
	
	
	
	
	
}
