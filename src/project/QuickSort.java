//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)

package project;

import java.util.ArrayList;
import java.util.Collections;

public class QuickSort {
	
	
	public static ArrayList<Item> sort(ArrayList<Item> array) {
		quickSort(array, 0, array.size() - 1);
		
		return array;
	}
	
	public static void quickSort(ArrayList<Item> a, int left, int right) {
		if (left < right) {
			int p = partition(a, left, right);
			quickSort(a, left, p-1);
			quickSort(a, p+1, right);
		}
	}
	
	public static int partition(ArrayList<Item> a, int left, int right) {
		Item pivot = a.get(right);
		
		int i=left-1;
		for (int j=left; j<=right; j++) {
			if (a.get(j).name.compareTo(pivot.name) < 0) {
				i++;
				Collections.swap(a, i, j);
			}
		}
		
		Collections.swap(a, i+1, right);
		return i+1;
	}
	
	

	
	
	
	

}
