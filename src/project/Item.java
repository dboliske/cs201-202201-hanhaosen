//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)



package project;

public class Item {// this class is to include all info for general items

	protected String name;
	protected double price;
	
	public Item() {
		this.name="pencil";
		this.price=1.29;
	}
	public Item(String name,double price) {
		this();
		setName(name);
		setPrice(price);
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Item   "+name+"    Price    "+price;
	}
	
	public String toCSV() {  // to write into a csv file
		return name+","+ price;
	}
	@Override
	public boolean equals(Object obj) {
		
		if (obj==null) {
			return false;
		}else if(! (obj instanceof Item) ) {
			return false;
		}
		
		Item i=(Item)obj;
		if (!name.equalsIgnoreCase(i.getName())) {
			return false;
		}else if(price!=i.getPrice()) {
			return false;
		}
		return true;
		
	}
	
	
}
