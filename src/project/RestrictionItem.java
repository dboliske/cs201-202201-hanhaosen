//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)



package project;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class RestrictionItem extends Item { // inherited from Item class, to include all info needed for restriction item

	private int ageRes;
	
	
	public RestrictionItem() {
		super();
		ageRes=0;
	}
	
	public RestrictionItem(String name, double price, int ageRestriction) {
		super(name,price);
		ageRes=ageRestriction;
	}

	public int getAgeRes() {
		return ageRes;
	}

	public void setAgeRes(int ageRes) {
		if (ageRes>0) {
		
		this.ageRes = ageRes;
		}
	}
	@Override
	public String toString() {
		return super.toString()+"    RestrictionAge    "+ageRes;
	}
	
	@Override
	
	public String toCSV() { // to write into a CSV file
		return super.toCSV()+","+ageRes;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) {
			return false;
		}else if(!(obj instanceof RestrictionItem)) {
			return false;
		}
		RestrictionItem p =(RestrictionItem)obj;
		if (this.ageRes!=p.getAgeRes()) {
			return false;
		}
		return true;
	}
	
	public boolean isEligible(Scanner input) { // to judge whether the customer permitted to buy the item
		
		
		System.out.println("Type in customer's age: ");
		input=new Scanner(System.in);
		String i=input.nextLine();
	  
	    try{
	    	int i2=Integer.parseInt(i);
	  
	   
		if (i2<ageRes) {
			
			return false;
			
		}else if(i2==ageRes) {
			System.out.println("Type in customer's date of birth in format MM/dd/yyyy: ");
			
			String birthdate=input.nextLine();
			
			try {
			String[] value=birthdate.split("/");
			
			Date d1=new Date(
				Integer.parseInt(value[0]),	
				Integer.parseInt(value[1]),
				(Integer.parseInt(value[2])+ageRes)
					);			
			LocalDate todaydate=LocalDate.now();
			DateTimeFormatter myformat=DateTimeFormatter.ofPattern("MM-dd-yyyy");
			String datenow=todaydate.format(myformat);
			Date d2=new Date(datenow);
			
			if (d1.before(d2)||d1.equals(d2)) {
				
				return true;
				
			}else {
				
				return false;
			}
			}catch(Exception e) {
				System.out.println("Wrongly format! Please use the MM/dd/yyyy format");
				return false;
			}
		}	
	}catch(Exception e) {
		System.out.println("Wrongly input");
		return false;
	}
	      
	     return true;
	
	}
	
	
}
