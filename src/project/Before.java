package project;

public interface Before {
    
	public abstract boolean before(Object obj);
	
}
