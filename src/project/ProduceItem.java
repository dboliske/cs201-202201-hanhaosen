//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)



package project;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ProduceItem extends Item {// ProduceItem class is set to include  all info needed for the produce products 

	private Date expirationDate;
	
	ProduceItem(){
		super();
		expirationDate=new Date();
	}
	
	ProduceItem(String name,double price,Date d){
		super(name,price);
		expirationDate=d;
	}
	
	ProduceItem(String name,double price,String date){
		super(name,price);
		try {
		expirationDate=Date.parseDate(date);
		}catch(Exception e) {
			
		}
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		
		Date newdate=Date.parseDate(expirationDate);
		this.expirationDate = newdate;
	}
	
	
	@Override
	public String toString() {
		return super.toString()+"    ExpirationDate    "+expirationDate;
		}
	
	@Override
	public String toCSV() {//  this method is for writing into a csv file
		return super.toCSV()+","+expirationDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}else if(!(obj instanceof ProduceItem)) {
			return false;
		}
		ProduceItem p =(ProduceItem)obj;
		if (!this.expirationDate.equals(p.getExpirationDate())) {
			return false;
		}
		return true;
	}
	
	public boolean fresh() {// to judge whether items's best-before date is before or after today's date
		
		LocalDate todaydate=LocalDate.now(); // introduce today's date
		DateTimeFormatter myformat=DateTimeFormatter.ofPattern("MM-dd-yyyy");
		String datenow=todaydate.format(myformat);
		Date d=new Date(datenow);
		if (d.before(this.expirationDate)) {
			return true;
		}else {
			return false;
		}
	}
	
	
}
