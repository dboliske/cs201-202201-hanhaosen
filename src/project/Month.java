//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)

package project;



public enum Month {// this enum is to enumerate all the months and days in each month

	JANUARY(31),
	FEBRUARY(28),
	MARCH(31),
	APRIL(30),
	MAY(31),
	JUNE(30),
	JULY(31),
	AUGUST(31),
	SEPTEMBER(30),
	OCTOBER(31),
	NOVEMBER(30),
	DECEMBER(31);
	
	private int days;
	
	Month(int days){
		this.days=days;
	}
	
	public int daysOfMonth() {
		
		return days;	
	}
	
	

	
	public static int daysOfMonth(int m) {
	     switch(m) {
	     case 1:
	    	 return 31;
	     case 2:
	    	 return 28;
	     case 3:
	    	 return 31;
	     case 4:
	    	 return 30;
	     case 5:
	    	 return 31;
	     case 6:
	    	 return 30;
	     case 7:
	    	 return 31;
	     case 8:
	    	 return 31;
	     case 9:
	    	 return 30;
	     case 10:
	    	 return 31;
	     case 11:
	    	 return 30;
	     case 12:
	    	 return 31;
	     }
	     return -1;
	}
	
	public static int getMo(Month m) {
		switch (m) {
			case JANUARY:
				return 1;
			case FEBRUARY:
				return 2;
			case MARCH:
				return 3;
			case APRIL:
				return 4;
			case MAY:
				return 5;
			case JUNE:
				return 6;
			case JULY:
				return 7;
			case AUGUST:
				return 8;
			case SEPTEMBER:
				return 9;
			case OCTOBER:
				return 10;
			case NOVEMBER:
				return 11;
			case DECEMBER:
				return 12;
		}
		
		return -1;
	}
}