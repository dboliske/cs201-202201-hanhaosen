//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;

package labs.lab1;

import java.util.Scanner;   // import scanner 

public class exercise2 {

	public static void main(String[] args) {
		
		//Task:  Your age subtracted from your father's age
		
		
		
		Scanner input=new Scanner(System.in);   	// create a variable for reading in user input
		
		System.out.print("Your age:   ");  // prompt the user to type in something
		
		int x=Integer.parseInt(input.nextLine()); // read in the next line of text typed by the user
		                                         //read in string and convert the data type to integer
		System.out.print("Your father's age:  ");
		
	    int y=Integer.parseInt(input.nextLine());
	    
	    System.out.print("Your age subtracted from your father's age is "+ (y-x)+ ".");// do the calculation and print out the result
		
	
		
		input.close(); // close input

	}

}
