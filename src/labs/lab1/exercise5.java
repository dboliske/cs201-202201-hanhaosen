//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;

package labs.lab1;

import java.util.Scanner;

public class exercise5 {

	public static void main(String[] args) {
		
	
//Convert your height in inches to feet and inches where inches is an integer (mode operator)
		
		
		
		
		
	    Scanner input=new Scanner(System.in);// create a variable for reading in user input

		
		System.out.print("Your height in inches:  ");  // prompt the user to type in 
		
		double x=Double.parseDouble(input.nextLine());  // read in the next line of text typed by the user
                                                         //read in string and convert the data type to double
 
		
		int  i=(int)(x/12);                                 // to get the feet number in the data type of integer

		                        
		
		int j=(int) Math.round(x-12*i);                      // to get the inch number in the data type of integer :round up to the nearest integer.
	
		
		System.out.print("Your height is "+i+" feet "+j+" inch.");  //  show the result
		
		
		
		input.close(); // close the input
	}

}
