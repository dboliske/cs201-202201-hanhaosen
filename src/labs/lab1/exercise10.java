//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;


package labs.lab1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class exercise10 {

	public static void main(String[] args) throws FileNotFoundException {
		
//      Task:	
//		Write a program that will do the following:
//		Prompt the user for the length, width, and depth in inches of a box.
//		Calculate the amount of wood (square feet) needed to make the box.
//		Display the result to the screen with a descriptive message.
//		Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.
		
		
		
		Scanner input=new Scanner(System.in);                             // use Scanner to read in
		System.out.print("Input a length(inches) for a  box: ");
		
		double x=Double.parseDouble(input.nextLine());
		
		System.out.print("Input a width(inchese) for a box:   ");
		
		double y=Double.parseDouble(input.nextLine());
		 
		System.out.print("Input a depth(inches) for a box:   ");
		
		double z= Double.parseDouble(input.nextLine());
		
		
		double v=x/12*y/12*z/12; // calculate the volumn(in cubic feet) of the box
		
		System.out.printf("The amount of wood(square feet) needed to make the box:  %.2f feet.",v); // format double to 2 decimal places
		
		input.close();
		
		// the following will use the date from the file("src/labs/lab1/box_length_width_depth") as the test table to test the program
		
		
		System.out.println();  // to separate the two program in the console
		
		
		
		
		File file=new File("src/labs/lab1/box_length_width_depth");
		Scanner input2=new Scanner(file);
		
		do {
			
			
			
			String data=input2.nextLine();
		
			double i=Double.parseDouble(data.substring(0,4));  // to collect the length number from the test table
	
		
			double j=Double.parseDouble(data.substring(5,9));
			
		
		    double k=Double.parseDouble(data.substring(10));
			
		
		    double vol=i/12*j/12*k/12; // calculate the volumn(in cubic feet) of the box
		    
			
			System.out.printf("(Test Table Data)  The amount of wood(square feet) needed to make the box is %.2f feet.",vol);
			System.out.println();
			
		}while(input2.hasNextLine());
		
		
		input2.close();
	}

}
