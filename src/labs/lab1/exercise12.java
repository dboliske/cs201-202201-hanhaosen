//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;


package labs.lab1;

import java.io.File;

import java.io.IOException;
import java.util.Scanner;

public class exercise12 {

	public static void main(String[] args) throws IOException {
		
		
//      Task: 	
//		Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.
//		Write a program that will convert inches to centimeters:
//		Prompt the user for inches.
//		Convert inches to centimeters
//		Display the result to the console.
//		Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.
				
		
		// use data from file ("src/labs/lab1/weather") 
		
		        File file=new File("src/labs/lab1/weather");
				Scanner input=new Scanner(file);
				
				
				while(input.hasNextLine()) {
					
					System.out.print("(Table data) Conver inches to centimeters\n inches:  ");
					
					String data=input.nextLine();
					System.out.print(data);
					
					double i=Double.parseDouble(data);
					
					double j=i*2.54;
					
					System.out.println("   centimeters: "+j);
					System.out.println();
					
					
				}
				
				 input.close();

	}

}
