//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;

package labs.lab1;

import java.io.File;

import java.io.IOException;
import java.util.Scanner;

public class exercise9 {

	public static void main(String[] args) throws IOException {
		
		
//		Test your program with various temperatures: low, high, middle. 
//		Use a test table with as many rows as needed to test your program. 
//		Are you satisfied that your program works as expected? 
//	    Submit your test plan and its results.
//		
				
		
		File file=new File("src/labs/lab1/weather");  // to insert a file for reading
		Scanner input=  new Scanner(file);
		
		double lowest =300;     // insert a number big enough that the file has a number small than it.
		double highest=-300;
		

		    while(input.hasNextLine()) {        // use while loop to find the lowest temperature
			String temp1=input.nextLine();
			double temp2=Double.parseDouble(temp1);
		    if (temp2<=lowest) {
				lowest=temp2;
			}		
		    }
		    System.out.println("lowest temperature is  "+lowest); // close the input
		    input.close();
	    
		    
		    
		  
			  Scanner input2=  new Scanner(file);    // use input2 to as distinguished to input,in order to to rescan the file
              while(input2.hasNextLine()) {
        	  String temp3=input2.nextLine();
        	  double temp4=Double.parseDouble(temp3);
        	  if (temp4>=highest) {
        		 highest=temp4;
        	 };
             }
              System.out.println("highest temperature is "+highest);
		     input2.close();
              
              
		     
		     
		     Scanner input3= new Scanner(file);      // use input3 to rescan the file
		     while(input3.hasNextLine()){	 
             double a=Double.parseDouble(input3.nextLine());
             if((a<highest)&&(a>lowest)) { 
             System.out.println("middle temperature :  "+a);
             }	
		     }
		     input3.close();
		

	}
}

