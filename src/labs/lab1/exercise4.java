//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;


package labs.lab1;

import java.util.Scanner;

public class exercise4 {

	public static void main(String[] args) {
	
		
		//Convert your height in inches to cms
		
		
		Scanner input=new Scanner(System.in);// create a variable for reading in user input

		
		System.out.print("Your height in inches:  ");  // prompt the user to type in 
		
		double i=Double.parseDouble(input.nextLine());  // read in the next line of text typed by the user
                                                         //read in string and convert the data type to double
 
		
		double j=2.54*i;                                //create double j to do the calculation
		
		System.out.print("Your height in centimeters: "+j+" cm.");  //  show the result
		
		
		input.close();   // close the input

	}

}


