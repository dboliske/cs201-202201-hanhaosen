//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)

//Exercise3
//Create two instance variables, name (a String) and strength (a double).
//Write the default constructor.
//Write the non-default constructor.
//Write two accessor methods, one for each instance variable.
//Write two mutator methods, one for each instance variable.
//Write a method that will return the entire as a single string (the toString method).
//Write a method that will return true if the strength is between 0 and 10.
//Write a method that will compare this instance to another Potion (the equals method).
//Now write an application class that instantiates two instances of Potion. 
//One instance should use the default constructor and the other should use the non-default constructor. 
//Display the values of each object by calling the toString method.



package labs.lab4;

public class Potion {

	private String name;
	private double strength;     
	
	
	
	
	public Potion() {   // default Constructor
		 
		name="***";
		strength=0.0;
		
	}
	
	
	
	public Potion(String name, double strength) { // non-default Constuctor
		
		this.name="***";
		setName(name);
		
		this.strength=0.0;
		setStrength(strength);
		
	}
	
	
	public String getName() {   // accessor methods
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	
	public void setName(String name) {  // mutator methods
		this.name=name;
	}
	
	
	public void setStrength(double strength) {  
		
		if(strength>0 && strength<10) {
		
		this.strength=strength;
		}
	}
	
	public String toString() {      // toString methods
		return name+" : "+strength;
	}
	
	public boolean validStrength(double strength) {        // check valid strength
		if (strength>0 && strength <10) {
			return true;
		}
		   return false;
	}
	
	

	
	
	public boolean equals(Potion p) {              // equal methods
		if (!this.name.equalsIgnoreCase(p.getName())) {
			return false;
		} else if (this.strength!=p.getStrength()) {
			return false;
		} 
		    return true;
	}
	
	
	
	
	
	
	
	
	
}
