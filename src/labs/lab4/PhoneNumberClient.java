//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)



// Exercise 2
//Part II: PhoneNumber Class

//Write the class, PhoneNumber.java following this UML Diagram and doing the following:

//Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
//Write the default constructor.
//Write the non-default constructor.
//Write 3 accessor methods, one for each instance variable.
//Write 3 mutator methods, one for each instance variable.
//Write a method that will return the entire phone number as a single string (the toString method).
//Write a method that will return true if the areaCode is 3 characters long.
//Write a method that will return true if the number is 7 characters long.
//Write a method that will compare this instance to another PhoneNumber (the equals method).
//Now write an application class that instantiates two instances of PhoneNumber. 
//One instance should use the default constructor and the other should use the non-default constructor.
//Display the values of each object by calling the toString method.




package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
	
		
		PhoneNumber  n1=new PhoneNumber();
		
		System.out.println(n1.toString());
		
		System.out.println(n1.validAreaCode("100"));
	
		n1.setCountryCode("1");
		n1.setAreaCode("333");
		n1.setNumber("666666");
		
		System.out.println(n1.toString());
		System.out.println(n1.getCountryCode());
		System.out.println(n1.getAreaCode());
		System.out.println(n1.getNumber());
		
		
		
		
		
		
		PhoneNumber n2=new PhoneNumber("333","4444","7777777"); // input invalid areacode and valid number
		
		System.out.println(n2.toString());
		System.out.println(n2.getCountryCode());		
		System.out.println(n2.getAreaCode());
		System.out.println(n2.getNumber());
		
		
		
		
	   System.out.println(n1.validNumber("1234567"));
	   System.out.println(n2.validAreaCode("123"));
		
	
		
		System.out.println(n1.equals(n2));
		
		
		
		
		
		
		
		

	}

	private static char[] validNumber(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	private static void valid(String string) {
		// TODO Auto-generated method stub
		
	}

}
