//HAOSEN HAN; CS201; SEC 3 ; Master of Computer Science(Beacon Education China Online)


// Exercise 1
//	Part I: GeoLocation Class

//	Write the class, GeoLocation.java following this UML Diagram and doing the following:

//	GeoLocation UML

//	Create two instance variables, lat and lng, both of which should be doubles.
//	Write the default constructor.
//	Write the non-default constructor.
//	Write 2 accessor methods, one for each instance variable.
//	Write 2 mutator methods, one for each instance variable.
//	Write a method that will return the location in the format "(lat, lng)" (the toString method).
//	Write a method that will return true if the latitude is between -90 and +90.
//	Write a method that will return true if the longitude is between -180 and +180.
//	Write a method that will compare this instance to another GeoLocation (the equals method).
//	Now write an application class that instantiates two instances of GeoLocation.
//	One instance should use the default constructor and the other should use the non-default constructor. 
//	Display the values of the instance variables by calling the accessor methods.


// not use try ()exception methods?


package labs.lab4;

public class GeoLocation {  // create two instance variables, both be double

	private double lat;
	private double lng;
	
	
	public GeoLocation() {  // write the default constructor
		
		lat=0.0;
		lng=0.0;

	}
	

    public GeoLocation(double lat,double lng ) {  // non-default constructor
    	
    	this.lat=lat;
    
    	setLat(lat);
    	
    	this.lng=lng;
         
    	setLng(lng);
    	
    }
	
	
	public double getLat() {  //  create accessor
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	
	
	public void setLat(double lat) { // create mutator
		 
		
		if ((lat>=-90)&&(lat<=90)) {
		this.lat=lat;
		}
	}
	
	
	public void setLng(double lng) {
		if((lng>=-180)&&(lng<=180)) {
		this.lng=lng;
	}
	}
	
	
	
	
	public String toString() {
		
		return "("+ lat+" , "+lng+")";
		
		
	}
	
	
	
	
	public boolean validLat(double lat) {  // create validLat to judge whether lat is valid 
		
		if (lat>=-90 && lat<=90) {
			
			return true;
		}
		 return false;
	}	
	
	
	
	
	
	public boolean validLng(double lng) {
		
		if(lng>=-180 && lng<=180 ) {
			
			return true;
		} 
		return false;
	}
	

	
	
	public boolean equals(GeoLocation g) {
		
		if (Math.abs(this.lat-g.getLat())> 0.00001) {     // 0.00001 in decimal degree in lat or lng is about 1.1m to 0.4 m in length, 
			                                              // we assume equal if the result  less that.
			return false;
			
		} else if (Math.abs(this.lng-g.getLng())>0.00001) {
			
			return false;
		}
		return true;
	}
	
	
	
     public double calcDistance(GeoLocation g) {  //	Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2)).
		
		return Math.sqrt(Math.pow(this.lat-g.getLat(),2)+ Math.pow(this.lng-g.getLng(),2));
	}
	
	
	
	  public double calcDistance(double lat, double lng) {
	  	
		
		return Math.sqrt(Math.pow(this.lat-lat,2)+Math.pow(this.lng-lng, 2));
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
