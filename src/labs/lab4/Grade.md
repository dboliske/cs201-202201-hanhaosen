# Lab 4

## Total

22/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        6/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   1/1
* Documentation         3/3

## Comments

1. GeoLocation doesn't validate before setting instance variables in either the non-default constructor or the mutator methods and validation methods do not follow the UML diagram
2. PhoneNumber doesn't validate in the non-default constructor and the validation methods do not follow the UML diagram
3. Potion doesn't validate before setting instance variables in either the non-default construtor or the mutator methods and the validation methods do not follow the UML diagram.
