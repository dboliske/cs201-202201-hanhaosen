//HAOSEN HAN; CS201; SEC 3 ; Master of Computer Science(Beacon Education China Online)



// Exercise2 
//Part II: PhoneNumber Class

//Write the class, PhoneNumber.java following this UML Diagram and doing the following:

//Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
//Write the default constructor.
//Write the non-default constructor.
//Write 3 accessor methods, one for each instance variable.
//Write 3 mutator methods, one for each instance variable.
//Write a method that will return the entire phone number as a single string (the toString method).
//Write a method that will return true if the areaCode is 3 characters long.
//Write a method that will return true if the number is 7 characters long.
//Write a method that will compare this instance to another PhoneNumber (the equals method).
//Now write an application class that instantiates two instances of PhoneNumber. 
//One instance should use the default constructor and the other should use the non-default constructor.
//Display the values of each object by calling the toString method.





// not use try ()exception methods?

package labs.lab4;

public class PhoneNumber {
	
	
	private String  countryCode;
	private String  areaCode;
	private  String  number;
	
	
	public PhoneNumber() {
		
		countryCode="###";
		
		areaCode="###";
		
		number="########";

	}
	
	public PhoneNumber(String cCode, String aCode, String number) {
		
		countryCode="###";
		
		setCountryCode(cCode);
	    
		areaCode="###";
		
	    setAreaCode(aCode);
	    
	    
		
		this.number="#######";
		setNumber(number);

	}
	
	
	public String getCountryCode() {  // accessor
		return countryCode;
	}
	
	public String getAreaCode() {
		  return areaCode;
	}
	
	public String getNumber() {
		  return number;
	}

	
	
	public void setCountryCode(String cCode) {
		 countryCode=cCode;
	}
	
	public void setAreaCode(String aCode) {
	
		if(aCode.length()==3) {
	     areaCode=aCode;
    }
	}	
	
	public void  setNumber(String number) {
		
		 
		if (number.length()==7) {
			this.number=number;
		}
		
	}
	
	
	
	
	
	public String toString() {
		return countryCode+"-"+areaCode+"-"+number;
	}
	
	
	
	public boolean validAreaCode(String aCode) {
		
		if (aCode.length()!=3) {
			return false;
		}
		    return true;
	}
	
	
	
	
	public boolean validNumber(String number) {
		
		if (number.length()!=7) {
			return false;
		}
		    return true;
	}
	
	
	public boolean equals(PhoneNumber pn) {
		
		if (!this.countryCode.equals(pn.getCountryCode())) {
			return false;
		} else if (!this.areaCode.equals(pn.getAreaCode())) {
			return false;
		} else if(! this.number.equals(pn.getNumber())) {
			return false;
		}  return true;
		
	}
	
	
	
	
}
