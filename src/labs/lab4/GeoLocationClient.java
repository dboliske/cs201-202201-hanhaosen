//HAOSEN HAN; CS201; SEC 3 ; Master of Computer Science(Beacon Education China Online)


// Exercise 1
//	Part I: GeoLocation Class

//	Write the class, GeoLocation.java following this UML Diagram and doing the following:

//	GeoLocation UML

//	Create two instance variables, lat and lng, both of which should be doubles.
//	Write the default constructor.
//	Write the non-default constructor.
//	Write 2 accessor methods, one for each instance variable.
//	Write 2 mutator methods, one for each instance variable.
//	Write a method that will return the location in the format "(lat, lng)" (the toString method).
//	Write a method that will return true if the latitude is between -90 and +90.
//	Write a method that will return true if the longitude is between -180 and +180.
//	Write a method that will compare this instance to another GeoLocation (the equals method).
//	Now write an application class that instantiates two instances of GeoLocation.
//	One instance should use the default constructor and the other should use the non-default constructor. 
//	Display the values of the instance variables by calling the accessor methods.




package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		
		
		GeoLocation loc1=new GeoLocation();  // use default Constructor
		
		System.out.println(loc1.toString());
		
		
		System.out.println(loc1.getLat());
		
		System.out.println(loc1.getLng());
		
		
	    loc1.setLat(55.55555);
	    loc1.setLng(88.88888);
		
		System.out.println(loc1.toString());
		
		System.out.println(loc1);
		
		
		
		GeoLocation loc2=new GeoLocation(30,30); // use non-default Constructor
		
		
		System.out.println(loc2.getLat());
		System.out.println(loc2.getLng());
		
		System.out.println(loc2.toString());
		
		
		
		System.out.println(loc1.equals(loc2));  // check equals
		
		loc2.setLat(55.555552);
		loc2.setLng(88.888882);
		
		
		System.out.println(loc1.equals(loc2));  // check equals after adjustment
		
		
		GeoLocation loc3=new GeoLocation(400,400);  // input invalid lat and lng
		
		System.out.println(loc3.toString());  // since I did not add parameter limitation to the setter and add setter to the non-default Constructor
		                                     //  it will shown what typed in.
		
		
		System.out.println(loc3.validLat(88.88));  // check whether lat is valid
		
		System.out.println(loc3.validLng(999));
		
		System.out.println(loc2.validLat(90));
		
		System.out.println(loc2.validLng(180));
		
		

	}

}
