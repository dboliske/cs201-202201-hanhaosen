//HAOSEN HAN; CS201; SEC 3; Master of Computer Science(Beacon Education China Online)


//Exercise 3
//Create two instance variables, name (a String) and strength (a double).
//Write the default constructor.
//Write the non-default constructor.
//Write two accessor methods, one for each instance variable.
//Write two mutator methods, one for each instance variable.
//Write a method that will return the entire as a single string (the toString method).
//Write a method that will return true if the strength is between 0 and 10.
//Write a method that will compare this instance to another Potion (the equals method).
//Now write an application class that instantiates two instances of Potion. 
//One instance should use the default constructor and the other should use the non-default constructor. 
//Display the values of each object by calling the toString method.






package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		

		Potion p1=new Potion();       // default Constructor
		System.out.println(p1.toString());
		
		p1.setName("yellow");
		p1.setStrength(88.88);   //  check mutator function
		
		System.out.println(p1.toString()); 
		
		
		
		Potion p2=new Potion("red", 15);     // non-default Constructor
		System.out.println(p2.toString());
		System.out.println(p2.getName());   // check acessor function
		System.out.println(p2.getStrength());
		
		
		System.out.println(p1.equals(p2));   // check equals function
		
		
		

	}

}
