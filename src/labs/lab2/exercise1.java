//HAOSEN HAN; CS201; SEC 1;Master of Computer Science;


package labs.lab2;

import java.util.Scanner;

public class exercise1 {

	public static void main(String[] args) {
	
		
//      Task:
//		Write a Java program that will prompt the user for a number and print out a square with those dimensions. 
//		For example, if the user enters 5, return the following:
//		      * * * * *
//		      * * * * *
//		      * * * * *
//		      * * * * *
//		      * * * * *
		
		
		
		Scanner size=new Scanner(System.in);      //  import  scanner
		System.out.print("Enter square size:  "); // prompt the user to type in
		int t=Integer.parseInt(size.nextLine());    // change the data type from string to integer 
		
		for(int i=0;i<t;i++) {for(int j=0;j<t;j++) {  // use for loop to draw in the console
			System.out.print(" *");
		}   System.out.println();
		}
	
		size.close();
		
		
		
		
		
	}

}
