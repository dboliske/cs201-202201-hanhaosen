//HAOSEN HAN; CS201; SEC 2;Master of Computer Science;


package labs.lab2;

import java.io.File;

import java.io.IOException;
import java.util.Scanner;

public class exercise2b {

	public static void main(String[] args) throws IOException {
		
		
	
		
//		    Write a Java program that will prompt the user for the grades for an exam, 
//			computes the average, and returns the result. 
//			Your program must be able to handle an unspecified number of grades.	
//			Be sure to test your code with a variety of values and number of grades.
//			(Hint: Tell the user to enter -1 when they finished entering grades)
			
			
		
//		students name from file, not from user
		
		
		
			
			File file=new File("src/labs/lab2/name");   // insert exam name from a file
			
			Scanner input=new Scanner(file);
			
			double total=0.0;
			
			int n=0;
	
			System.out.println("Enter -1 to stop grading ");
			
			boolean flag =true;
			
			
			
				
			while(input.hasNextLine() && flag)  {
								
			String name=input.nextLine();
				
			System.out.print("Input a grade for "+name+" : "); // prompt user to type in a grade	  
			
			Scanner input2=new Scanner(System.in);// add another scanner to read what just typed in.    ??? Question: do know how to close input2 in a loop
			
			String grade=input2.nextLine();
			
			if (!grade.equals("-1")) {
			
			System.out.println("Grade for "+name+" : "+ grade);
			
			Double mark=Double.parseDouble(grade);
			
			total=mark+total;             // calculate the total
	
			n++;                          //update

			double average=total/n;      // calculate the average grade
			
			System.out.println("average grade of "+n+" students :"  +average); // average for all  will be shown on the second last name in the file
			
			}
			
			else {
				
				System.out.println("Exit");
				
				flag=false;
			
			}
			
			}
		
			input.close();
			
	}

}
