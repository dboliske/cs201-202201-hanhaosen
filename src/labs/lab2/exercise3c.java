//HAOSEN HAN; CS201; SEC 3;Master of Computer Science;




package labs.lab2;

import java.util.Scanner;

public class exercise3c {

	public static void main(String[] args) {
		
//	    Task
//		Write a Java program that will repeatedly display a menu of choices to a user and prompt them to enter an option.
//		You should use the following options:
//		Say Hello - This should print "Hello" to console.
//		Addition - This should prompt the user to enter 2 numbers and return the sum of the two.
//		Multiplication - This should prompt the user to enter 2 numbers and return the product of the two.
//		Exit - Leave the program

		
		
		// use while do loop
		
		
		
		Scanner input=new Scanner(System.in);  
	  	
		String choice="";   // to initiate the loop
		
		while(!choice.equals("4")) {
		
  	    
			System.out.println("1. print 'Hello");
		    System.out.println("2. Do addition");
		    System.out.println("3. Do multiplication ");
		    System.out.println("4. Exit");
		    System.out.print("Type in your choice: ");
		    choice=input.nextLine();  //get user choice
		    
		    switch(choice) {   //handle user menu option
		    
		    case "1":
		    	System.out.println("Hello");
		    	break;
		    case "2":
		    	System.out.println("Do addition. \nEnter first number: ");
		    	String m=input.nextLine();
		    	double x=Double.parseDouble(m);
		    	System.out.println("Enter second number: ");
		    	String n=input.nextLine();
		    	double y=Double.parseDouble(n);
		        System.out.println(x+"+"+y+"="+(x+y));
		        break;
		    
		    case "3":
		    	System.out.println("Do multiplication. \nEnter first number: ");
		    	String m1=input.nextLine();
		    	double i=Double.parseDouble(m1);
		    	System.out.println("Enter second number: ");
		    	String n1=input.nextLine();
		    	double j=Double.parseDouble(n1);
		        System.out.println(i+"*"+j+"="+(i*j));
		        break;
		        
		    case"4":
		    	System.out.println("Exit");	
		    
		    	break;
		    	
		    default:
		    	System.out.println("Not recognized");
		    
		    } 
		}
	        
	        input.close();		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
