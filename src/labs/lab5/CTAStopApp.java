//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)




package labs.lab5;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import labs.lab4.GeoLocation;

public class CTAStopApp {

	public static void main (String[] args) throws FileNotFoundException {
		
		String filename="src/labs/lab5/CTAStops.csv";
		
		CTAStation[] data= readFile(filename);
		
		menu(data);
		
		
		
	}
	
	

	public static CTAStation [] readFile(String filename) throws FileNotFoundException {
		
		
		CTAStation [] data =new CTAStation[10];
		int count=0;
		
		 File f= new File(filename);
		 Scanner input=new Scanner(f);
		 
		while(input.hasNextLine()) {
			
		   String line=input.nextLine();
			
		   String[] value=line.split(",");
		   
		   
		   
		    try{
		        	
		   
		    
		    CTAStation c=new CTAStation(
			
		    value[0],
		    Double.parseDouble(value[1]),
		    Double.parseDouble(value[2]),
		    value[3],
		    Boolean.parseBoolean(value[4]),
		    Boolean.parseBoolean(value[5]));
		    
		    data[count]=c;	
			
			if(count==data.length-1) {
				
				data=resize(data,2* data.length);
			}
			
			
		    count++;
		 

		    }
		    catch(Exception e) {
		    	
		    //	System.out.println(e.getMessage());
		   // to avoid the error from the top row which are catalogs 
		    }

		}
		 
		
		 input.close();
		 
		 data=resize(data,count);
		 
		
		
		 return data;
	}
	


	public static CTAStation [] resize(CTAStation[] d, int size) {
		
		
		
		int limit=d.length<size?d.length:size;
		
		CTAStation [] temp=new CTAStation[size];
		
		for(int i=0;i<limit;i++) {
			
			temp[i]=d[i];
		}
		
		
		
		return temp;
		
		
		
	}
	
	
	
	
	
	public static void menu(CTAStation [] stations) {
		
		
		
		Scanner input=new Scanner(System.in);
		boolean flag=true;
		
		do {
			
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.println("what's your choice:  ");
			
			
			String choice=input.nextLine();
			
			switch(choice) {
			
			case"1":
				displayStationNames(stations);
				break;
				
			case"2":
				displayByWheelchair(input,stations);
				break;
				
			case"3":
				displayNearest(input,stations);
				break;
				
			case"4":	
				System.out.println("See you next time!  ");
			 flag=false;
			 break;
			 
			 default:
				 
				 System.out.println("Sorry, your input is invalid. ");
				 break;
			}	
			
		}while(flag);
		
		

		input.close();
		
	}
	

	public static void displayStationNames(CTAStation[] stations) {
		
		
		for(int i=0;i<stations.length;i++) {
			
		
			if (stations[i]!=null) {
			
		 System.out.println(stations[i].getName());
		}
		}
		
	}
	
	

	
	@SuppressWarnings("resource")
	public static void displayByWheelchair(Scanner input, CTAStation [] stations) {
		
		
		
		boolean flag=true;
		
		while(flag) {
		
		System.out.println("Search wheelchair accessible stations ? Type in 'y' or 'n' " );
	
			
		input=new Scanner(System.in);
			
		String c=input.nextLine();
			
		switch(c) {
		
		case "y":
			
			int count1=0;
			
			for(int i=0;i<stations.length;i++) {
				
				if (stations[i].hasWheelchair()) {
					
					System.out.println(stations[i].getName());
					
					count1++;
				} 

			}
			
			if (count1==0) {
				
				System.out.println("Sorry, there are no wheelchair accessible stations.  "); // Display a message if no stations are found
			}
					
		
		return;
		
		case "n":
			
			int count2=0;
			
			for(int i=0;i<stations.length ;i++) {
				
				if (!stations[i].hasWheelchair()) {
					
					System.out.println(stations[i].getName());
					
					count2++;
				} 
				
			}
			if (count2==0) {
				
				System.out.println("Sorry, no stations that are wheelchair inaccessible. ");  // Display a message if no stations are found
			}
			

        
		return;
		
		
		
		default:
			System.out.println("Sorry, your input is invalid. ");
			
		}
		
		}
		
		input.close();
		
	}
	
	


	
	public static void displayNearest(Scanner input, CTAStation[] stations) {
		
		System.out.print("Type in latitude:  ");
		
		double lat=Double.parseDouble(input.nextLine());
		
        System.out.print("Type in longitude:  ");
		
		double lng=Double.parseDouble(input.nextLine());
		
		
		
		GeoLocation p=new GeoLocation(lat, lng);
		
		GeoLocation [] g=new GeoLocation[stations.length];
		
	    Double [] d=new Double [stations.length];
	    
	
		
		for(int i=0;i<stations.length ;i++) {
			
			 
			
			g[i]=new GeoLocation(stations[i].getLat(),stations[i].getLng()); 
			d[i]=p.calcDistance(g[i]);
			
		}
		
	
		double min=d[1];
		
		for (int i=0;i<d.length;i++) {
			
			if (min>=d[i]) {
				
				min=d[i];
			
			}
		
		}
		
		for (int i=0;i<stations.length;i++) {
			
			if (d[i]==min) {
				
				System.out.println("Nearest Station: "+ stations[i].getName());
			}
		}

		
	}
	
	
	
	
	
	
	
	
}
