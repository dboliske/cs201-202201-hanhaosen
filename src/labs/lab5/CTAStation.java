//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)

package labs.lab5;

import labs.lab4.GeoLocation;

public class CTAStation extends GeoLocation {

	
	private String name;
	
	private String location;
	
	private boolean wheelchair;
	
	private boolean open;
	
	
	
	public CTAStation () {
		
		super();
		
		this.name="Harlem";
		
		this.location="elevated";
		
		this.wheelchair=true;
		
		this.open=true;
		

	}
	
	
	public CTAStation (String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		
		super(lat,lng);
		
		this.name="Harlem";
		
	    setName(name);
		
		this.location="elevated";
		
		setLocation(location);
		
		this.wheelchair=true;
		
		setWheelchair(wheelchair);
			
		this.open=true;
		
		setOpen(open);
		
	
	}
	
	
	
	
	public String getName() {
		
		return this.name;
	}
	
	 
	public String getLocation() {
		
		return this.location;
	}
	
	
	public  boolean hasWheelchair() {
		
		return this.wheelchair;
	}
	
	
	public  boolean isOpen() {
		
		return this.open;
	}
	
	
	
	public String setName(String name) {
		
		return this.name=name;
	}
	
	
	public  String setLocation (String location) {
		
		return this.location=location;
	}
	
	
	public boolean setWheelchair(boolean wheelchair) {
		
		return this.wheelchair=wheelchair;
	}
	
	
	public boolean setOpen(boolean open) {
		
		return this.open=open;
	}
	
	
	
	
	@Override
	
	public String toString() {
		
		return name+ super.toString()
		+" in  "+this.getLocation()+", "
		+this.hasWheelchair()+ " wheelchair accessible,  "
		+this.isOpen()+ " open. " ; 
	}
	
	
	
	@Override
	
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) {
			return false;
		}
		else if (!(obj instanceof CTAStation)){
			return false;
		}
		
		
			CTAStation o=(CTAStation) obj;
			
			if (!this.getName().equals(o.getName())) {
				
				return false;
			}else if(!this.getLocation().equals(o.getLocation())) {
				
				return false;
			}else if(this.hasWheelchair()!=o.hasWheelchair()) {
				
				return false;
			}else if(this.isOpen()!=o.isOpen()) {
				return false;
			}
		
		   return true;		
		
	}
	
	
	
	
	
	
	
}
