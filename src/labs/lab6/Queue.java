//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)



package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Queue {

	
    public static ArrayList<String>  menu(Scanner input, ArrayList<String> data) {
    	
    	
    	
    	String [] options= {"1. Add customer to queue", 
    						"2. Help customer", 
    						"3. Exit", 
    						"4. Check customer number ",
    						"5. List all customer ",
    						"What's your option?  "};
    	
    	boolean flag=true;
	
    	do {
    	
    	for (int i=0;i<options.length;i++) {
    		
    		System.out.println(options[i]);
    	}
    	
    	
    	String op=input.nextLine();
    	
    	switch(op) {
    	
    	case"1":
    		data=addCustomer(input,data);
    		break;
    		
    	case"2":
    		data=helpCustomer(input,data);
    		break;
    		
    	case"3":
    		System.out.println("Goodbye! ");
    	    flag=false;
    	    break;
    	    
    	case"4":
    		checkNumber(input,data);
    		break;
    	    
    	case"5":
    		listAllCustomers(data);
    		break;
    	default:
 		
    		System.out.println("Sorry, option not recognised");
    	
    	}
 	
    	
    	}while(flag);
    	
    	return data;
    }

	
	
	
	
	public static ArrayList<String> addCustomer(Scanner input, ArrayList<String> data){
		
		System.out.println("Customer name: ");
		
		String name=input.nextLine();
		
		data.add(name);
		
		System.out.println("Number of customer : "+data.indexOf(name));
			
		return data;
	}
	
	
	public static ArrayList<String> helpCustomer(Scanner input, ArrayList<String>data){
		if (data.size()!=0) {
		System.out.println("Customer "+data.get(0)+" please!");
		
		data.remove(0);
		} else {
			System.out.println("There is no customer in line");
		}
		
		return data;
	}
	
	
	public static void checkNumber(Scanner input,ArrayList<String> data) {
		
		System.out.println("Enter customer name: ");
		
		String name=input.nextLine();
		int m=0;

		for (int i=0;i<data.size();i++) {
			
			if (name.toLowerCase().equals(data.get(i).toLowerCase())) {
				
				System.out.println("Number for Customer "+name+" is "+i);
				m=-1;
			}
		
		}
		if(m==0) {
			System.out.println("Sorry, Customer "+name+" is not in queue");
		}
		
	}
	

	
	public static void listAllCustomers(ArrayList<String> data) {
		
		for(String name: data) {
			
			System.out.println(data.indexOf(name)+" "+name);
		}
		
		
	}
	
	
	
	public static void main(String[] args) {

      Scanner input=new Scanner(System.in);
      
      ArrayList<String> data=new ArrayList<String>();
   
      data=menu(input,data);
      
      input.close();
		
		
	}

}
