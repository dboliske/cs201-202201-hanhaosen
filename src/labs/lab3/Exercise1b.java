//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)



package labs.lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Exercise1b {

	public static void main(String[] args) throws FileNotFoundException {
		

		// Exercise 1		
		//You have been given a file called "src/labs/lab3/grades.csv". It contains a list of students and their exam grades.
		//Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
			
		
		
		// method2:  create a corresponding array to solve
		
		
		File f=new File("src/labs/lab3/grades.csv");	
		Scanner  input=new Scanner(f);
		double[] g=new double[5];  // start array  with length 5, since the students number is more than 5	
		int count=0;
		
		
		
		
		
		while(input.hasNextLine()) {          	
			String[]line=input.nextLine().split(",");	
			g[count]=Double.parseDouble(line[1]);		
			count++;   
			
			    if (count==g.length) {  // establish a new array with a bigger length		
				double[]b=new double[2*g.length];			
				for(int i=0;i<g.length;i++) {					
					b[i]=g[i];
				}
				g=b;      // array g now has a bigger length and original corresponding value.
				b=null;   // put an end to the useless bigger array.
			}				
		}
		
		
		
		
		
		
		  double total=0;
		  double[]s=new double[count-1];  //  trim array g to a corresponding array.	
		  for (int i=0;i<count-1;i++) {	  
			  s[i]=g[i];
			  total=total+s[i];
		  }
			 
		  	
		  
		  
		  
			 
		   double average=total/(count-1); 		 
		   System.out.printf("Average grade: %.2f ", average); // display double in 2 decimal places
	       input.close();
		
		

	}

}
