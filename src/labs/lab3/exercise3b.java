//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)



package labs.lab3;


import java.util.Arrays;

public class exercise3b {

	public static void main(String[] args) {
		

// exercise 3
		
//      Write a program that will find the minimum value and print it to the console for the given array:
//		{72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110,
//		101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115,
//		101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 
//		32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}
		
		
		
        int []a={72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		

		
		int min=a[0];
		
		for(int i=0;i<a.length;i++) {   // use the a.length to avoid out of array bounds array
			
			if(a[i]<=min) {
				min=a[i];

		}
		}
		
		System.out.println("minimum number in the array is "+min);
		
		
		
		
		
		
		int [] value=new int[a.length];  // create an array for the index
		
		int count=0;
		
     	for(int i=0;i<a.length;i++){
     		
     		if (a[i]==min) {
     			
     			value[count]=i;
     			
     			count++;
     		}
     		
     	}
		
     	
     	
     	
     	
     	
       int [] index=new int [count];  // trim the array to a corresponding size
       
       for(int i=0;i<count;i++) {
    	   
    	   index[i]=value[i];
   
       }
     	  
     	
     	 System.out.println("the numeric index of the min number in the array are:   ");
     	 
         System.out.println(Arrays.toString(index));  // print the index array
	
		
		
		
		
		
		
		
		

	}

}
