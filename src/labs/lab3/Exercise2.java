//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)


package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) throws IOException {
		

//      Exercise 2
//		Write a program that will continue to prompt the user for numbers,
//		storing them in an array, until they enter "Done" to finish, 
//		then prompts the user for a file name so that these values can be saved to that file. For example,
//		if the user enters "output.txt", 
//		then the program should write the numbers that have been read to "output.txt".
		
		
		
		
		Scanner input=new Scanner(System.in);
		
		
		
		System.out.println("Enter 'Done' to finish " );
		
	
		boolean flag=true;
		int [] n=new int[2];
		int count=0;    //  to control valid array length
		
		for(int i=0;flag;i++) {  // use flag control and for loop to prompt unlimited number
			
			count++;
			System.out.print("Enter a number: ");
			
						
			String number=input.nextLine();
			
		
			
			try{
				n[i]=Integer.parseInt(number);
			
		       System.out.println("("+i+") number: "+number);
			
		    if (i==n.length-1) {  // to initialize a bigger array 
		    	
		    	int []bigger=new int[4*i];
		    	for(int j=0;j<=i;j++) {
		    		
		    		bigger[j]=n[j];
		    		
		    	 }
		    	   n=bigger;
		    	   bigger=null;
		       } 
		    
			}catch(Exception e) {  // use exception to ensure program continue with invalid input
				
				if(number.equalsIgnoreCase("Done")) {
					
					System.out.println("Exit");
					
					flag=false;
				}
				
				else {
					System.out.println("Invalid Input");	
				}
				
			}
			
		}

		      System.out.println("Enter a name for a file which would store the number:");
		
		      String name=input.nextLine();
		       
		      String address="src/labs/lab3/"+name+".txt";
		      
		      FileWriter f=new FileWriter(address); // prepare a file for writing
		
		      for(int i=0;i<count;i++) {
		    	  
		    	  if (n[i]!=0) { 
		    	  f.write(n[i]+"\n");   // cannot distinguish 0 and valid input 0??
		    	  }
		    	  else {
		    	  f.write("invalid input"+"\n");  
		    	  }
		      }
	          f.flush();
	          f.close();
		      
		      input.close();
		      System.out.println("Finished");
		
	}

}
