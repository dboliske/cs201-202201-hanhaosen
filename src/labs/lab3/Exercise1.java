//Haosen Han;  CS 201;  Sec 1;  Master of Computer Science  (Beacon Online)


package labs.lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) throws FileNotFoundException {
		
		
		
// Exercise 1		
//You have been given a file called "src/labs/lab3/grades.csv". It contains a list of students and their exam grades.
//Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
		
		
		
		File f=new File("src/labs/lab3/grades.csv"); // create file for reading in
		
		Scanner input=new Scanner(f); // create scanner to start to read in
		
		int count=1;

		double total=0;  
		
	
		// loop start
		
	    while(input.hasNextLine()) {
	    	
	    	
	    	String[] line=input.nextLine().split(",");
	    	
	    	double[] g=new double [count];  
	    	
	    	g[count-1]=Double.parseDouble(line[1]);  // count from the 1st , one by one
	    	
	    	total=total+g[count-1];//  Even that a new array g is declared in every loop, 
	    	                       // we do not care about a new array but make sure the total is added each time.
	    	count++;
	    	
	    	
	    }
		
		
		double average=total/(count-1); // count-1 is the actual student number
		System.out.printf("average grade: %.2f ", average);//display double in 2 decimal places
		
		
		input.close();
		
		
		
		
		
		
		
		
	}

}
