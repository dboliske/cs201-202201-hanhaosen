//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)

package labs.lab7;

public class ExerciseThree {

	
	public static double[] sort(double[] data) {
		
		for(int i=0;i<data.length-1;i++) {
			
			int min=i;
			
			for(int j=i+1;j<data.length;j++) {
				
				if (data[j]<data[min]) {
					
					min=j;
				}
			}
	
			if (min!=i) {
				
			swap(data,i,min);
			
			}
		}
		
		return data;
	
	}
	
	
	
	
	public static void swap(double []a, int i,int j) {
		
		
            if ((i>=0)&& (i<a.length) && (j>=0) && (j<a.length)) {
			
			double temp=a[i];
			a[i]=a[j];
			a[j]=temp;
		}
	
	}
	
	
	public static void main(String[] args) {
		
		
		double []d={3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		d=sort(d);
		for(double i: d) {
			
			System.out.print(i+" ");
		}
		
	}
	
	
	
	
	
}
