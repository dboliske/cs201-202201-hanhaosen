//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)

package labs.lab7;

public class ExerciseTwo {

	public static String[] sort(String[] data) {
		
		for(int i=0;i<data.length;i++) {
			
			int j=i;
			
		while(j>0 && data[j].compareTo(data[j-1])<0) {
			
			String temp=data[j];
			data[j]=data[j-1];
			data[j-1]=temp;
			j--;
		}
		}
		return data;
	}
	
	public static void main(String[] args) {
		
		String[] a={"cat", "fat", "dog", "apple", "bat", "egg"};
		
		a= sort(a);
		
		for(String i: a) {
			System.out.print(i+" ");
		}
		
		
	}
	
	
}
