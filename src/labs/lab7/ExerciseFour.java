//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)


package labs.lab7;

import java.util.Scanner;

public class ExerciseFour {

	public static void search(String[] data, String value) {
		
         search(data, value, 0, data.length);
	}
	
	
	public static void search(String[]data, String value, int start, int end) {
		
	
		int middle=(start+end)/2;
		
		if (middle==start||middle==end) {
			if (data[middle].equals(value)) {
				System.out.print(value+" found at index "+middle);
			}
			else {
				System.out.println(value+" not found ");
			}
			return;
		}
		
		if (data[middle].compareTo(value)<0) {
			search(data,value,middle,end);
		}
		else if(data[middle].compareTo(value)>0) {
			search(data,value,start,middle);
		}
		else if(data[middle].equals(value)) {
			System.out.print(value+" found at index "+middle);
		}
		
		
	}
	
	   public static void main(String[]args) {
		   
		   String[] a={"c", "html", "java", "python", "ruby", "scala"};
		   
	       a=ExerciseTwo.sort(a);  // to ensure the array is sorted before search
		   
		   Scanner input=new Scanner(System.in);
		   
		   System.out.println("Search value:  ");
		   
		   String value=input.nextLine();
		   
		   search(a,value);
	 
		   input.close();
	   }


}
