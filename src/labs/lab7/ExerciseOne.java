//Haosen Han;  CS 201;  Sec 3;  Master of Computer Science  (Beacon Online)



package labs.lab7;

public class ExerciseOne {
	
	public static int [] sort(int[] data) {
		
		boolean flag=true;
		
		do {
			
		flag=false;
		for(int i=0;i<data.length-1;i++) {
			
			if(data[i+1]<data[i]) {
				swap(data, i,i+1);
				flag=true;
			}
		}
		}while(flag);
		return data;
		
	}
	
	public static void swap(int [] a, int i, int j) {
		
		if ((i>=0)&& (i<a.length) && (j>=0) && (j<a.length)) {
			
			int temp=a[i];
			a[i]=a[j];
			a[j]=temp;
		}
		 
	}
	
     public static void main(String[]args) {
    	 
    	 int[] data={10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
    	 
    	 data=sort(data);
    	 
    	for(int i:data) {
    		System.out.print(i+" ");
    	}
    	 
     }
	
	
	
}
