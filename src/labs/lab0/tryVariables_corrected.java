//Haosen Han;  CS 201;  
//Sec 1; Jan 25 2022; 
//Master of Computer Science


package labs.lab0;

public class tryVariables_corrected {

	public static void main(String[] args) {
		
		
	
		

				int integer = 100;
				long large = 42561230L;
				int  small = 25;   // lack of int
				byte tiny = 19;

				float f = .0925F;  // lack of semicolon ;
			    double decimal = 0.725; 
				double largeDouble = +6.022E23; 

				char character = 'A';
				boolean t = true;

				System.out.println("integer is " + integer);
				System.out.println("large is " + large);
				System.out.println("small is " + small); // capitalize S in system
				System.out.println("tiny is " + tiny);//  should be tiny , not tine
				System.out.println("f is " + f);
				System.out.println("decimal is " + decimal); // should be dote instead of comma
				System.out.println("largeDouble is " + largeDouble);
				System.out.println("character is "+ character);// lack of +
				System.out.println("t is " + t);

		
		
		
		
		
		
		
		
		
		
		

	}

}
